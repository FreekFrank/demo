package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT,
		
		classes = DemoApplication.class)
//@TestPropertySource(
//		  locations = "classpath:application-integrationtest.properties")
public class DemoApplicationIntegrationTests {
    @LocalServerPort
    private int port;
    
    @Value("${oauth.resource:http://localhost}")
    private String baseUrl;
    @Value("${oauth.authorize:/oauth/authorize}")
    private String authorizeUrl;
    @Value("${oauth.token:/oauth/token}")
    private String tokenUrl;
    
    
    protected OAuth2ProtectedResourceDetails resource() {

        ResourceOwnerPasswordResourceDetails resource = new ResourceOwnerPasswordResourceDetails();

        List scopes = new ArrayList<String>(2);
        scopes.add("write");
        scopes.add("read");
        resource.setAccessTokenUri("http://localhost:"+port+tokenUrl);
        resource.setClientId("demo");
        resource.setClientSecret("secret");
        resource.setGrantType("password");
        resource.setScope(scopes);

        resource.setUsername("peter@example.com");
        resource.setPassword("password");

        return resource;
    }

    public OAuth2RestOperations restTemplate() {
        AccessTokenRequest atr = new DefaultAccessTokenRequest();
        DefaultOAuth2ClientContext c = new DefaultOAuth2ClientContext(atr);
        return new OAuth2RestTemplate(this.resource(), c);
    }
    
    
	@Test
	public void contextLoads() {
	}
	
	
	
	@Test
    public void retrieveToken() throws Exception {
		OAuth2RestOperations restTemplate = this.restTemplate();
		
		OAuth2AccessToken acc = restTemplate.getAccessToken();
		System.out.println("AccessToken="+acc.getValue());
		
		
//		String token = this.restTemplate.getForObject
//        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/oauth/token", String.class), ).contains("Hello World");
    }

}
